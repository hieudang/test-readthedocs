FAQ
====================

Do your extensions support multiple domains?
-------------------------------------------

Yes, you can.

How to upgrade an extension?
------------

Download latest version of extension in My Account – My Downloads. Then you can follow this wiki Version Upgrade for further upgrading.

Can I request trial?
-------------

Our extensions are not provided with trial versions. However, we offer 30-day–money-back-guarantee to ensure customers’ right and benefit. You can consider the commercial version as a 30-day trial not only without encryption but also even more stabilization.

When you will install an extension to my store?
-----------------------------------------------

After purchase an extension included Installation service, we will process install the extension **within 24 hours** in workday and 48-72 hours in weekend.

Installation
-----------------------------------------------

* Upload module via ftp
* Go to admin clear cache

> If you find any JS/Jquery conflict or CSS issues, please submit a ticket to support team in Technical Department