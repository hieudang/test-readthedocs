
License
-------------------------------------------

This License is entered by Magecheckout to govern the usage or redistribution of Magecheckout software. This is a legal agreement between you (either an individual or a single entity) and Magecheckout for Magecheckout software product(s) which may include extensions, templates and services.

By purchasing, installing, or otherwise using Magecheckout products, you acknowledge that you have read this License and agree to be bound by the terms of this Agreement. If you do not agree to the terms of this License, do not install or use Magecheckout products.

The Agreement becomes effective at the moment when you acquire software from our site or receive it through email or on data medium or by any other means. Magecheckout reserves the right to make reasonable changes to the terms of this license agreement and impose its clauses at any given time.

* GRANT OF LICENSE

> Customer will receive source code open 100%.
> 
> Customer will obtain a License Certificate which will remain valid until the Customer stops using the Product or until Magecheckout terminates this License because of Customer’s failure to comply with any of its Terms and Conditions. 
> 
> Each License Certificate includes a license serial which is valid for one live Magento installation only and unlimited test Magento installations.
> 
> You are allowed to customize our products to fit with your using purpose.

* DESCRIPTION OF OTHER RIGHTS AND LIMITATIONS

> Installation and Use
> 
> For each new Software installation, you are obliged to purchase a separate License. 
> 
> You are not permitted to use any part of the code in whole or part in any other software or product or website.
> 
> You are legally bound to preserve the copyright information intact including the text/link at bottom.
