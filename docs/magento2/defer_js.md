Defer Javacsript
====================

User Guide
-------------------------------------------

**Magento Defer JavaScript Extension speeds up Magento store performance by automatically deferring your JavaScript loading**

Magento Defer JavaScript Extension is a useful extension that speeds up Magento store performance by automatically putting JavaScript files to the end of the page after loading all HTML and CSS. Magento Deferring JavaScript is listed as one of the most impact performance enhancements you can make among many other website optimization tips


`Mage::helper('deferjs')->deferjs($html)`

Compatibility
------------

1. *Community*: 1.4.x.x, 1.5.x.x, 1.6.x.x, 1.7.x.x, 1.8.x.x, 1.9.x.x
2. *Enterprise*: 1.12.x.x, 1.13.x.x, 1.14.x.x


Changelog
-------------

Update later....


Frequently Asked Questions
-----------------------------------------------

Update later....

Recommend Themes
-----------------------------------------------

* RWD
