MultiStorePricing
====================

User Guide
-------------------------------------------

**Magento Multistore Pricing extension by BSSCommerce is a useful tool which allows online shop owners to set and display different prices for each product in each store view of the corresponding store as wish.**

Magento Multistore Pricing extension by BSSCommerce is a useful tool which allows online shop owners set and display different prices for each product in each store view of the corresponding store as wish. With this powerful Magento extension, now you will not face with any trouble in setting specific prices in multi store views for each same product. Therefore, you can attract specific customers from different store views by price policy as you want.

Compatibility
------------

1. *Community*: 1.4.x.x, 1.5.x.x, 1.6.x.x, 1.7.x.x, 1.8.x.x, 1.9.x.x
2. *Enterprise*: 1.12.x.x, 1.13.x.x, 1.14.x.x


Changelog
-------------

Update later....


Frequently Asked Questions
-----------------------------------------------

Update later....

Recommend Themes
-----------------------------------------------

* RWD
